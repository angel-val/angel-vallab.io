+++
title = "projects - angel-val"
[extra]
cssdir = ".."
+++

# Projects

## Software

{% arrowed_textbox() %}
To see my software projects, you can take a look at my [gitlab](https://gitlab.com/angel-val).
None of them are really that impressive right now, but if I make something noteworthy I'll be sure to put it here :)
{% end %}
